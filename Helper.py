import sqlite3


class DatabaseUtils(object):

    @staticmethod
    def establish_connection(path_to_database):
        conn = sqlite3.connect(path_to_database)
        return conn

    @staticmethod
    def close_connection(conn):
        conn.close()

