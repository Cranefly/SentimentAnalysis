import bs4 as bs
import urllib.request
# import pandas as pd

# TODO: Extract information from here: https://www.focus-bikes.com/au_en/archive/
# TODO: Section "product-geometry" might also be interesting.

def get_soup(web_page):
    """Get BeautifulSoup object for a given wep page."""
    sauce = urllib.request.urlopen(web_page).read()
    soup = bs.BeautifulSoup(sauce, 'lxml')
    return soup


def extract_bicycle_groups(web_page):
    """Extract the pages of the main bicycle groups."""
    list_of_links = []

    soup = get_soup(web_page)

    # Going down the stack.
    for element in soup.find_all('li', 'bike-category-header'):
        for ul in element.ul:
            if type(ul) == bs.element.Tag:
                list_of_links.append(ul.find_all('a')[0].get('href'))

    return list_of_links


def get_bicycles_for_group(web_page):
    """Extract a list of bicycles for a given link to a group."""
    list_of_bicycles = []

    # Open in between web page.
    soup = get_soup(web_page)
    print("Processing: " + str(soup.title))
    link_to_bicycles = soup.find_all('a', 'btn-focus btn-focus-reverse')[0].get('href')

    # Extract links to bicycles of specific group.
    soup = get_soup(link_to_bicycles)
    [list_of_bicycles.append(link.get('href')) for link in soup.find_all('a', 'product-link')]

    return list_of_bicycles


def extract_configuration(web_page):
    """Extract information about single bicycle."""

    import sys
    print(sys.version)

    # dfs = pd.read_html(web_page)
    # for df in dfs:
    #     print(df)

    # soup = get_soup(web_page)
    # soup = soup.find_all('section', 'product-specification')[0]
    # table = soup.find('table')
    # table_rows = table.find_all('tr')
    #
    # for tr in table_rows:
    #     td = tr.find_all('td')
    #     row = [i.text for i in td]
    #     print(row)


def main():
    # main_page = 'https://www.focus-bikes.com/au_en/'
    # links_to_bicycle_groups = extract_bicycle_groups(main_page)
    # print(link) for link in links_to_bicycle_groups]
    # links_of_bicycles = get_bicycles_for_group(links_to_bicycle_groups[0])
    # [print(link) for link in links_of_bicycles]

    link_to_bicycle = 'https://www.focus-bikes.com/au_en/24439-izalco-max-disc-ultegra-di2.html'
    extract_configuration(link_to_bicycle)



if __name__ == "__main__":
    main()


# import scrapy
#
#
# class BlogSpider(scrapy.Spider):
#     name = 'blogspider'
#     start_urls = ['https://www.focus-bikes.com/au_en/']
#
#     def parse(self, response):
#         for title in response.css('h2.entry-title'):
#             yield {'title': title.css('a ::text').extract_first()}
#
#         next_page = response.css('div.prev-post > a ::attr(href)').extract_first()
#         if next_page:
#             yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

