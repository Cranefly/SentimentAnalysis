import random
from settings import *                      # Contains global settings like path for data storage.
from Helper import *                        # Includes function which can be used in multiple projects.
import json                                 # Save data
from unidecode import unidecode             # Unicode transformations


class ImageInformation(object):
    """The information about the comments shall be extracted here. Moreover, these are preprocessed for later use."""

    # TODO: Later also other variables of the comments can be used to simulate higher-dimensional data.

    def __init__(self):
        self.comments = []          # Is a list of dictionaries.
        self.dictionary_keys = ['id', 'sentiment', 'original_length', 'text']

    def __getstate__(self):
        pass

    def display_comments(self, number):
        """Randomly displays a given number of comments."""
        for i in range(number):
            print(random.choice(self.comments))

    def get_labeled_comments(self):
        """Get labeled comments from a database. Labeled comments are the ones which contain a clear label (three 
        ratings in one dimension)."""
        extracted_comments = []
        new_sql_statement = 'SELECT score.comment_id, score.score, comment.comment_text ' \
                            'FROM score ' \
                            'INNER JOIN comment ON score.comment_id=comment.id ' \
                            'WHERE score.score = 1 OR score.score = -1'

        # Establish a connection to the database.
        conn = DatabaseUtils.establish_connection(global_path_to_database)
        c = conn.cursor()

        # Iterate through all comments which are labeled.
        for row in c.execute(new_sql_statement):

            # Create dictionary entry.
            self.add_entry_to_comment_dictionary(row)

        # Close connection to database.
        DatabaseUtils.close_connection(conn)

    def add_entry_to_comment_dictionary(self, row):
        """Adds an entry to the class variable, which stores the information about comments in a dictionary."""
        if row[1] == -1:
            # No hate.
            sentiment = 0
        else:
            # Hateful.
            sentiment = 1

        keys = self.dictionary_keys
        self.comments.append({keys[0]: row[0], keys[1]: sentiment, keys[2]: len(row[2]), keys[3]: row[2]})

    @staticmethod
    def remove_whitespace(text):
        """Remove unnecessary white space from text."""
        text = ' '.join(text.split())
        return text

    @staticmethod
    def standardize_string_length(string, length):
        """Standardize the string length to a given length."""
        if len(string) == length:
            return string
        elif len(string) < length:
            return string.ljust(length, ' ')
        elif len(string) > length:
            return string[0:length]

    def randomize_order(self):
        """Randomize the order of comments."""
        random.shuffle(self.comments)

    @staticmethod
    def replace_special_characters(text):
        """"""
        transformed = unidecode(text)
        transformed.encode('utf-16')
        return transformed

    def format_comments(self):
        """Format all comments. More functions can be added."""
        for i in range(len(self.comments)):

            keys = self.dictionary_keys

            # Remove whitespace
            comment = self.comments.pop(i)
            comment_text = self.remove_whitespace(comment[self.dictionary_keys[3]])

            # Replace umlauts
            comment_text = self.replace_special_characters(comment_text)

            # Normalize length of texts.
            comment_text = self.standardize_string_length(comment_text, 500)

            # Save results in class variable.
            self.comments.append(
                {keys[0]: comment[keys[0]], keys[1]: comment[keys[1]], keys[2]: comment[keys[2]],
                 keys[3]: comment_text})

    def save_comments(self):
        """Save the formatted comments with json-format."""
        with open(global_path_to_reformatted_comments + '.json', 'w') as f:
            json.dump(self.comments, f)


def main():

    data = ImageInformation()
    data.get_labeled_comments()
    data.format_comments()
    data.randomize_order()
    data.save_comments()
    data.display_comments(10)


if __name__ == "__main__":
    main()


# # # Old code # # #

# # Get number of rated comments
# c.execute('SELECT COUNT(*) FROM score WHERE score.score = 1 OR score.score = -1')
# number_of_rated_comments = c.fetchone()[0]


# dictionary = OrderedDict()

# # Create dictionary.
# for i in comments:
#     dictionary[i.id] = [i.sentiment, i.original_length, i.text]

# # Save and pickle the created dictionary.
# with open(global_path_to_reformatted_comments + '.pickle', 'wb') as f:
#     pickle.dump(dictionary, f, pickle.HIGHEST_PROTOCOL)
