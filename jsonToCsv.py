import json

"""Extract information about the comments from json and save it as .csv file."""

input_file = '../data/reformattedComments.json'
output_file = '../data/reformattedComments.csv'

with open(input_file, 'r') as handle:
    comments = json.load(handle)

# Get labels and ids
labels = []
ids = []
texts = []

for i in range(len(comments)):
    ids.append(comments[i]['id'])
    labels.append(comments[i]['sentiment'])
    texts.append(comments[i]['text'])

f = open(output_file, 'w', encoding="utf-8")
for i in range(len(labels)):
    string = str(labels[i]) + ';' + str(ids[i]) + ';' + texts[i].replace(';', ' ').replace('\n', ' ') + '\n'
    f.write(string)
