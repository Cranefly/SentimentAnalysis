import json
import numpy as np
import matplotlib.pyplot as plt
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from sklearn import manifold

# TODO: Later also other variables of the comments can be used to simulate higher-dimensional data.


def main():
    """The given comments are displayed."""

    filename = '../data/tsne2.pckl'
    input_file = '../data/reformattedComments.json'
    desired_length = 1500

    ids, comments, labels = get_information(input_file, desired_length)
    # comments = [comment['text'] for comment in comments]
    # comments = np.array(comments)
    # distance_matrix = tsne(comments)
    #
    # save_to_file(filename, distance_matrix)
    x = open_model(filename)

    plot_embedding(x, labels, ids)


def get_information(input_file, desired_length):
    """Get labels and comment text. Save both in the class variable."""
    with open(input_file, 'r') as handle:
        comments = json.load(handle)

    # Get labels and ids
    labels = np.zeros(len(comments))
    ids = np.zeros(len(comments))
    for i in range(len(comments)):
        ids[i] = comments[i]['id']
        labels[i] = comments[i]['sentiment']

    # Get comment text
    [adjust_comment_length(comment, desired_length) for comment in comments]

    return ids, comments, labels


def adjust_comment_length(dictionary_entry, desired_length=4):
    """Adjust the length of a given string to the desired length."""
    text = dictionary_entry['text'][:desired_length]
    text = format(text, 'x<' + str(desired_length))
    dictionary_entry['text'] = text
    return dictionary_entry


def plot_embedding(x, y, ids, title=None):
    """Create the plot embedding and display the plot."""
    x_min, x_max = np.min(x, 0), np.max(x, 0)
    x = (x - x_min) / (x_max - x_min)

    plt.figure(figsize=(15, 15))
    ax = plt.subplot(111)
    # ax.xaxis.set_visible(False)
    # ax.yaxis.set_visible(False)
    for i in range(x.shape[0]):
        plt.text(x[i, 0], x[i, 1], str(ids[i].astype(int)),
                 color=plt.cm.Set1(y[i] / 10.),
                 fontdict={'weight': 'normal', 'size': 7})

    if title is not None:
        plt.title(title)

    plt.show()


def tsne(x):
    """Create tf.idf vector and distance matrix from the given data."""
    vectorizer = TfidfVectorizer(min_df=3, stop_words=stopwords.words('german'),
                                 strip_accents='unicode', lowercase=True, ngram_range=(1, 2),
                                 norm='l2', smooth_idf=True, sublinear_tf=False, use_idf=True)
    x = vectorizer.fit_transform(x)
    d = -(x * x.T).todense()  # Distance matrix: dot product between tfidf vectors

    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    x_tsne = tsne.fit_transform(d)

    return x_tsne


def save_to_file(filename, data):
    f = open(filename, 'wb')
    pickle.dump(data, f)
    f.close()


def open_model(filename):
    f = open(filename, 'rb')
    x_tsne = pickle.load(f)
    f.close()
    return x_tsne


if __name__ == "__main__":
    main()